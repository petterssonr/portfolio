import React, { Component } from 'react';
import { Link } from 'react-router';
import TopSection from './top-section';
import Work from './work';

class Home extends Component {

	constructor(props) {
	  super(props);
	
	  this.state = { route: 'Home' };
	}

	selectRoute(route) {
		switch(route) {
			case 'Work':
				this.setState({ route: 'Work' });
				break;
			case 'About':
				this.setState({ route: 'About' });
				break;
			default:
				this.setState({ route: 'Home' });
		}
	}

	render() {
		console.log(data);
		switch(this.state.route) {
			case 'Work':
				return(
					<div className="work">
						<section className="top-section">
							<div className="row menu">
								<div className="col-md-6 menu-work selected-pink">
									<h3 className="work-link" onClick={() => this.selectRoute('Work')}>Work</h3>
								</div>
								<div className="col-md-6 menu-about">
									<h3 className="about-link" onClick={() => this.selectRoute('About')}>About</h3>
								</div>
							</div>
						</section>
						<section className="work-section" id="work-section">

						</section>
					</div>
				);
				break;
			case 'About':
				return(
					<div className="about">
						<section className="top-section">
							<div className="row menu">
								<div className="col-md-6 menu-work">
									<h3 onClick={() => this.selectRoute('Work')}>Work</h3>
								</div>
								<div className="col-md-6 menu-about selected-blue">
									<h3 onClick={() => this.selectRoute('About')}>About</h3>
								</div>
							</div>
						</section>
						<section className="about-section">

						</section>
					</div>
				);
				break;
			default: 
				return(
					<div className="home">
						<section className="top-section">
							<div className="row menu">
								<div className="col-md-6 menu-work">
									<h3 onClick={() => this.selectRoute('Work')}>Work</h3>
								</div>
								<div className="col-md-6 menu-about">
									<h3 onClick={() => this.selectRoute('About')}>About</h3>
								</div>
							</div>
						</section>

					</div>
				);
		}
	}
}

const data = [
	{
		id: 'a-cartoon-collection',
		title: 'A Cartoon Collection',
		category: 'Illustrations',
		text: 'I would love to see these characters come to live in a childrens book, a motion graphic or something similar',
	},
	{
		id: 'an-owl',
		title: 'An Owl',
		category: 'Illustrations',
		text: 'Shapes, patterns, symbols, patterns, shapes, and so on.. :)',
	},
	{
		id: 'an-oxe',
		title: 'An Oxe',
		category: 'Illustrations',
		text: 'Shapes, patterns, symbols, patterns, shapes, and so on.. :)',
	},
	{
		id: 'a-beer-can',
		title: 'A Beer Can',
		category: 'Illustrations',
		text: 'Shapes, patterns, symbols, patterns, shapes, and so on.. :)',
	},
	{
		id: 'an-oxe',
		title: 'An Oxe',
		category: 'Illustrations',
		text: 'Shapes, patterns, symbols, patterns, shapes, and so on.. :)',
	},
	{
		id: 'a-beer-can',
		title: 'A Beer Can',
		category: 'Illustrations',
		text: 'Shapes, patterns, symbols, patterns, shapes, and so on.. :)',
	},
];

export default Home;