import React, { Component } from 'react';

class TopSection extends Component {

	render() {
		return(
			<section className="top-section">
				<div className="row menu">
					<div className="col-md-6 menu-work">
						Work
					</div>
					<div className="col-md-6 menu-about">
						About
					</div>
				</div>
			</section>
		);
	}
}

export default TopSection;