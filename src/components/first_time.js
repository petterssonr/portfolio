import React, { Component } from 'react';
import { Link } from 'react-router';

class FirstTime extends Component {
	render() {
		return(
			<div className="content-wrapper">
				<h2 className="main-title">Welcome Username!</h2>
				<h3 className="subtitle">We see this is your first time using Fortum Smart Charging.</h3>
				<h3 className="subtitle">Would you like us to show you around?</h3>
				<div className="btn-group">
					<Link to="/info"><button className="btn btn-success btn-lg">Yes, show me</button></Link>
					<Link to="/select_charger"><button className="btn btn-default btn-lg">No, skip</button></Link>
				</div>
			</div>
		);
	}
}

export default FirstTime;