import React, { Component } from 'react';

class Work extends Component {

	renderWorkList() {
		console.log(this.props.content)
		return this.props.content.map((item, i) => {
			return (
				<div
				key={ i }
				className={'col-md-4 col-xs-12 custom-col ' + item.id}
				>
					
				</div>
			);
		})
	}

	render() {
		return(
			<div className="row work-list-group">
				{this.renderWorkList()}			
			</div>
		);
	}
}

export default Work;