import { Link, browserHistory } from 'react-router';

export function selectCharger(charger) {
	// selectCharger is an ActionCreator, it needs to return an action,
	// an object with a type property.
	// browserHistory.push('/confirm_charger');

	return {
		type: 'CHARGER_SELECTED',
		payload: charger
	};
}

export function selectEffect(effect) {
	
	browserHistory.push('/confirm_charger');

	return {
		type: 'EFFECT_SELECTED',
		payload: effect
	};
}