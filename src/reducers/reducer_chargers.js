export default function() {
	return [
		{ 
			id: 1, 
			title: 'Charger A', 
			effects: ['3,6 kW', '7,4 kW', '11 kW', '22 kW'], 
			prices: ['free', '0,5 NOK/min', '0,7 NOK/min', '1 NOK/min'],
			status: 'available' 
		},
		{ 
			id: 2, 
			title: 'Charger B',
			effects: ['3,6 kW', '7,4 kW', '11 kW', '22 kW'], 
			prices: ['free', '0,5 NOK/min', '0,7 NOK/min', '1 NOK/min'],
			status: 'occupied' 
		},
		{ 
			id: 3, 
			title: 'Charger C', 
			effects: ['3,6 kW', '7,4 kW', '11 kW', '22 kW'], 
			prices: ['free', '0,5 NOK/min', '0,7 NOK/min', '1 NOK/min'],
			status: 'available' 
		},
		{ 
			id: 4, 
			title: 'Charger D', 
			effects: ['3,6 kW', '7,4 kW', '11 kW', '22 kW'], 
			prices: ['free', '0,5 NOK/min', '0,7 NOK/min', '1 NOK/min'],
			status: 'outoforder' 
		},
	]
}