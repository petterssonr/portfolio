import { combineReducers } from 'redux';
import ChargersReducer from './reducer_chargers';
import ActiveCharger from './reducer_active_charger';
import ActiveEffect from './reducer_active_effect';

const rootReducer = combineReducers({
  chargers: ChargersReducer,
  activeCharger: ActiveCharger,
  activeEffect: ActiveEffect
});

export default rootReducer;
