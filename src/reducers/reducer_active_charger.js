// State argument is not application state, only the state
// this reducer is responsible for.
export default function(state = null, action) { // If state comes in as undefined, set it to null (ES6 synta)
	switch(action.type) {
		case 'CHARGER_SELECTED':
			return action.payload;
		default:
			return state;
	}
}